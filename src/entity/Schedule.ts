export class Schedule {
    id: number;
    start: string;
    end: string;

    constructor(
        id: number,
        start: string,
        end: string
    ) {
        this.id = id;
        this.start = start;
        this.end = end;
    }
}
