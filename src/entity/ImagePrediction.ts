export class ImagePrediction {
  id: number;
  name: string;
  size: string;
  prediction: number;
  imgSrc: string;

  constructor(
    id: number,
    name: string,
    size: string,
    prediction: number,
    imgSrc: string
  ) {
    this.id = id;
    this.name = name;
    this.size = size;
    this.prediction = prediction;
    this.imgSrc = imgSrc;
  }
}
