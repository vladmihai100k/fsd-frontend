import axios from "axios";
import instance from "../api/instance";
import axiosInstance from "../api/instance";

export const login = async (email: string, password: string, setAuth: (auth: boolean) => void) => {
    try {
        const response = await axiosInstance.post("/login", { email: email, password: password });
        if (response.data.token) {
            localStorage.setItem("token", response.data.token);
            //localStorage.setItem("token", JSON.stringify(response.data.token));
            setAuth(true);
        }
        return response.data;
    } catch (err) {
        console.log(err);
        throw err;
    }
}

export const logout = (setAuth: (auth: boolean) => void) => {
    localStorage.removeItem("token");
    setAuth(false);
}