import { Stack } from "@mui/material";
import CalendarInfo from "./CalendarInfo";
import CalendarContent from "./CalendarContent";
import CalendarActions from "./CalendarActions";


const Calendar = () => {
    return (
        <Stack>
            <CalendarInfo />
            <CalendarContent />
            <CalendarActions />
        </Stack>
    );
}

export default Calendar;