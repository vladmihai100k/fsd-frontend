import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";

const CALENDAR = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]

const CalendarContent = () => {
    // @ts-ignore
    return (
        <div style={{  width: "600px",
            background: "linear-gradient(to right top, #4358cb, #c951bd)",
            borderRadius: "2.75%"}}>
            <TableContainer>
                <Table>
                    <CalendarHeader />
                    <CalendarBody />
                </Table>
            </TableContainer>
        </div>
    );
}

const CalendarHeader = () => {
    return (
        <TableHead>
            <TableRow>
                <TableCell >SU</TableCell>
                <TableCell>MO</TableCell>
                <TableCell>TU</TableCell>
                <TableCell>WE</TableCell>
                <TableCell>TH</TableCell>
                <TableCell>FR</TableCell>
                <TableCell>SA</TableCell>
            </TableRow>
        </TableHead>
    );
}

const CalendarBody = () => {

    const calendar = [];
    const preprocCal = CALENDAR.map(day => {
        return <TableCell >{day}</TableCell>
    });

    for (let index = 0; index < preprocCal.length / 7; index++) {
        calendar.push(<TableRow >{preprocCal.slice(index * 7, index * 7 + 7)}</TableRow>)
    }

    return (
        <TableBody>
            {calendar}
        </TableBody>
    );
}

export default CalendarContent;