import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: "http://localhost:3001",
    timeout: 1000,
    headers: {}
});

axiosInstance.interceptors.request.use(async (config) => {
    const token = localStorage.getItem("token");
    //const token = localStorage.getItem("token")?.slice(1, -1);
    if (config.headers) {
        config.headers.Authorization = token ? `Bearer ${token}` : '';
    }
    // if (token) {
    //     config.headers = { authorization: `Bearer ${token}` };
    // }
    return config;
})

export default axiosInstance;