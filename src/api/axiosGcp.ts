import axios from 'axios';

const axiosGcp = axios.create({
    baseURL: "https://us-central1-fsd-alex-andrei-dani-vlad.cloudfunctions.net",
    timeout: 20000,
    headers: {}
});

// axiosGcp.interceptors.request.use(async (config) => {
//     const token = localStorage.getItem("token");
//     //const token = localStorage.getItem("token")?.slice(1, -1);
//     if (config.headers) {
//         config.headers.Authorization = token ? `Bearer ${token}` : '';
//     }
//     // if (token) {
//     //     config.headers = { authorization: `Bearer ${token}` };
//     // }
//     return config;
// })

export default axiosGcp;