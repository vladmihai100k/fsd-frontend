import React, { useContext, useState } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import LoginPage from "./pages/LoginPage";
import TablePage from "./pages/TablePage";
import UploadPage from "./pages/UploadPage";
import DatePickerOldPage from "./pages/DatePickerOldPage";
import DatePickerPage from "./pages/DatePickerPage";
import "./resources/App.css";
import AuthContext from "./DataContexts";
import { Redirect } from "react-router";
import useLocalStorage from "./hooks/useLocalStorage";
import UsersPage from "./pages/UsersPage";
import { logout } from "./hooks/useAuth";

const App = () => {
  require("dotenv").config();
  console.log("API URL:" + process.env.API_URL);
  const [authenticated, setAuthenticated] = useLocalStorage(
    "authenticated",
    false
  );
  // const setAuthenticated = (a: boolean) => {
  //     console.log("in functie");
  // }
  // const authenticated = true;
  return (
    <AuthContext.Provider value={{ authenticated, setAuthenticated }}>
      <BrowserRouter>
        <Switch>
          <Route exact path={"/"}>
            {authenticated ? <Redirect to="/table" /> : <LoginPage />}
          </Route>
          <Route exact path={"/table"}>
            {/* <TablePage /> */}
            {authenticated ? <TablePage /> : <Redirect to="/" />}
          </Route>
          <Route exact path={"/upload"}>
            {/* <UploadPage /> */}
            {authenticated ? <UploadPage /> : <Redirect to="/" />}
          </Route>
          <Route exact path={"/date-picker-old"}>
            <DatePickerOldPage />
          </Route>
          <Route exact path={"/date-picker"}>
            <DatePickerPage />
          </Route>
          <Route exact path={"/users"}>
            <UsersPage />
          </Route>
          <Route exact path={"/logout"}>
            <Logout />
          </Route>
        </Switch>
      </BrowserRouter>
    </AuthContext.Provider>
  );
};

const Logout = () => {
  const { authenticated, setAuthenticated } = useContext(AuthContext);
  logout(setAuthenticated);
  return <Redirect to="/" />;
};

export default App;
