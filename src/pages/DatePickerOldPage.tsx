import {
  Box,
  Grid,
  Avatar,
  Stack,
  TextField,
  Typography,
  Button,
  Link,
} from "@mui/material";
import { color } from "@mui/system";
import Calendar from "../components/DatePicker/Calendar/Calendar";

const DatePickerOldPage = () => {
  return (
    <Grid
      container
      justifyContent="center"
      alignItems="center"
      direction="column"
      width={"100vw"}
      height={"100vh"}
      style={{
        //background: "linear-gradient(to right top, #4358cb, #c951bd)"
        background: "#ffffff",
      }}
    >
      <Stack direction={"row"} justifyContent="center" alignItems="center">
        <Button
          variant="contained"
          style={{
            backgroundColor: "#ffffff",
            color: "#000000",
            justifyContent: "flex-start",
          }}
          //size={"large"}
          sx={{ minWidth: "100%" }}
        >
          <div style={{ textAlign: "left" }}>
            <p style={{ fontSize: "20px", color: "purple" }}>Start date:</p>
            <p>2/12/2021</p>
          </div>
        </Button>
        <Button
          variant="contained"
          style={{
            backgroundColor: "#ffffff",
            color: "#000000",
            justifyContent: "flex-start",
          }}
          // size={"large"}
          sx={{ minWidth: "100%" }}
        >
          <div style={{ textAlign: "left" }}>
            <p style={{ fontSize: "20px", color: "blue" }}>End date:</p>
            <p>12/12/2021</p>
          </div>
        </Button>
      </Stack>
      <Calendar />
    </Grid>
  );
};

export default DatePickerOldPage;
