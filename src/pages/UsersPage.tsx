import { useEffect, useState } from "react";
import axiosInstance from "../api/instance";

const users = async () => {
    const response = await axiosInstance.get('/users');
    console.log("====================")
    console.log(response.data);
    console.log("====================")
    return response.data;
}

const UsersPage = () => {
    const [data, setData] = useState("");
    const [loading, setLoading] = useState(true);

    // useEffect with an empty dependency array works the same way as componentDidMount
    useEffect(() => {
        const func = async () => {
            try {
                // set loading to true before calling API
                setLoading(true);
                const data = await users();
                setData(JSON.stringify(data));
                // switch loading to false after fetch is complete
                setLoading(false);
            } catch (error) {
                // add error handling here
                setLoading(false);
                console.log(error);
            }
        };
        func();
    }, []);

    // return a Spinner when loading is true
    if (loading) return (
        <span>Loading</span>
    );

    // data will be null when fetch call fails
    if (!data) return (
        <span>Data not available</span>
    );

    // when data is available, it will be shown
    return (
        <div>
            {data}
        </div>
    );
};

export default UsersPage;