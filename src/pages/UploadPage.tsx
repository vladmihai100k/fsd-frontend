import avatarImage from "../images/avatar.png";
import { useState, useRef } from "react";
import { Grid, Button } from "@mui/material";
import FormData from "form-data";
import axiosInstance from "../api/instance";
import useLocalStorage from "../hooks/useLocalStorage";
import { ImagePrediction } from "../entity/ImagePrediction";

const UploadPage = () => {
  const [image, setImage] = useState("");
  const [file, setFile] = useState<File>();
  const [images, setImages] = useLocalStorage("images", JSON.stringify([]));
  const inputFile = useRef<HTMLInputElement>(null);
  const openFileDialog = () => {
    inputFile?.current?.click();
  };

  const onSave = async () => {
    if (file == null) {
      return;
    }
    let formData = new FormData();
    formData.append("image", file);
    await axiosInstance
      .post("evaluate", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((response) => {
        console.log(response.data);
        let imageList: Array<ImagePrediction> = JSON.parse(images);
        imageList.push(
          new ImagePrediction(
            imageList.length,
            file.name,
            file.size.toString(),
            response.data[0],
            image
          )
        );
        setImages(JSON.stringify(imageList));
      })
      .catch((err) => {});
  };

  const onImageChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files && event.target.files[0]) {
      setImage(URL.createObjectURL(event.target.files[0]));
      setFile(event.target.files[0]);
    }
  };

  return (
    <Grid
      direction={"column"}
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <Grid container justifyContent="flex-end">
        <Button
          variant={"contained"}
          color={"inherit"}
          style={{
            width: "10%",
            margin: "5%",
          }}
          onClick={openFileDialog}
        >
          Upload
        </Button>
        <Button
          variant={"contained"}
          color={"inherit"}
          style={{
            width: "10%",
            margin: "5%",
          }}
          onClick={onSave}
        >
          Save
        </Button>
      </Grid>
      <Grid
        container
        justifyContent={"center"}
        bgcolor={"#d1d1d1"}
        width={"90vw"}
        height={"60vh"}
        display="flex"
        alignItems="center"
      >
        <img src={image} alt={"image"} style={{ maxWidth: "100%" }}></img>
      </Grid>
      <input
        type="file"
        id="file"
        ref={inputFile}
        onChange={onImageChange}
        style={{ display: "none" }}
      />
    </Grid>
  );
};
export default UploadPage;
