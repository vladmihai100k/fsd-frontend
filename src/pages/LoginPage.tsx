import { useContext, useState } from "react";
import { Box, Grid, Avatar, Stack, TextField, Typography, Button, Link } from "@mui/material";
import MailIcon from "@mui/icons-material/Mail";
import LockIcon from "@mui/icons-material/Lock";
import avatarImage from "../resources/images/avatar.png";
import AuthContext from "../DataContexts";
import { login } from "../hooks/useAuth";

const LoginPage = () => {
    const { authenticated, setAuthenticated } = useContext(AuthContext);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    return (
        <Grid container
            // justifyContent="center"
            // alignItems="center"
            width={"100vw"}
            height={"100vh"}
            style={{
                background: "linear-gradient(to right top, #4358cb, #c951bd)"
            }}
        >
            <Grid container item
                bgcolor={"white"}
                margin={"80px"}
                mx={"200px"}
                borderRadius={5}
            >
                <Grid container item xs
                    justifyContent="center"
                    alignItems="center"
                >
                    <Box
                        borderRadius={50}
                        boxShadow={16}
                    >
                        <Avatar
                            src={avatarImage}
                            sx={{ width: 200, height: 200, padding: "50px" }}
                        />
                    </Box>
                </Grid>
                <Grid container item xs
                    justifyContent="center"
                    alignItems="center"
                >
                    <Stack width={"50%"}
                        spacing={2}
                        justifyContent="center"
                        alignItems="center"
                    >
                        <Typography variant={"h3"}
                            mb={5}>
                            User Login
                        </Typography>
                        <TextField
                            id="emailTextField"
                            value={username}
                            onChange={event => { setUsername(event.target.value) }}
                            variant="filled"
                            sx={{ minWidth: "100%" }}
                            InputProps={{
                                hiddenLabel: true,
                                disableUnderline: true,
                                startAdornment: <MailIcon />,
                                placeholder: " Email Id ",
                            }}
                        />

                        <TextField
                            id="passwordTextField"
                            value={password}
                            onChange={event => { setPassword(event.target.value) }}
                            type="password"
                            variant="filled"
                            sx={{ minWidth: "100%" }}
                            InputProps={{
                                hiddenLabel: true,
                                disableUnderline: true,
                                startAdornment: <LockIcon />,
                                placeholder: " Password"
                            }}
                        />
                        <Button variant="contained"
                            style={{ backgroundColor: "#54bc44" }}
                            size={"large"}
                            sx={{ minWidth: "100%" }}
                            onClick={() => {
                                //setAuthenticated(true);
                                //alert(username + " " + password);
                                login(username, password, setAuthenticated);
                            }
                            }>
                            Login
                        </Button>
                        <Link underline={"hover"}>
                            Forgot Username / Password?
                        </Link>
                    </Stack>
                </Grid>
            </Grid>
        </Grid>
    );
}


export default LoginPage;