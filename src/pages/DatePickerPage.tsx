import { Button, Grid } from "@mui/material";
import { useEffect, useState } from "react";
import DatePicker, { DayValue, DayRange, Day } from 'react-modern-calendar-datepicker'
import * as React from 'react';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import { DateRange, DateRangePicker, LocalizationProvider } from "@mui/lab";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { DataGrid } from "@mui/x-data-grid";
import { ImagePrediction } from "../entity/ImagePrediction";
import * as moment from 'moment';
import { format } from 'date-fns';
import axiosGcp from "../api/axiosGcp";
import { Schedule } from "../entity/Schedule";
import useLocalStorage from "../hooks/useLocalStorage";

const columns = [
  { field: "id", headerName: "ID", hide: true },
  { field: "start", headerName: "Start date", flex: 1 },
  { field: "end", headerName: "End date", flex: 1 },
];

const DatePickerPage = () => {
  const [value, setValue] = React.useState<DateRange<Date>>([null, null]);
  // const [rows, setRows] = useState<Array<Schedule>>([]);
  const [idCounter, setIdCounter] = useState<number>(0);
  //const [rowsJson, setRowsJson] = useLocalStorage("schedules", JSON.stringify([]));
  //let rows: Array<Schedule> = JSON.parse(rowsJson);
  let [rows, setRows] = useState<Array<Schedule>>([]);
  console.log("IdCounter from render: " + idCounter);
  // let rows: Array<Schedule> = [];

  useEffect(() => {
    axiosGcp.get("/getIntervals").then((response) => {
      console.log("Getting data from gcp");
      console.log(response.data);
      let schedules: Array<Schedule> = [];
      for (let index in response.data) {
        schedules.push(new Schedule(
          Number(index),
          response.data[index].start_date,
          response.data[index].end_date
        ));
      }
      setRows(schedules);
      //setRowsJson(JSON.stringify(schedules));
      setIdCounter(schedules.length);
      //console.log("IdCounter:" + idCounter);
    }).catch((err) => {
      console.log(err);
    })
  }, [])

  const submit = () => {
    if (value[0] !== null && value[1] !== null) {
      //rows = Object.assign([], rows);
      let newRows = Object.assign([], rows);
      let schedule: Schedule = new Schedule(
        idCounter,
        format(value[0], 'MM/dd/yyyy'),
        format(value[1], 'MM/dd/yyyy')
      );
      setIdCounter(idCounter + 1);
      console.log("IdCounter from submit: " + idCounter);
      //rows.push(schedule);
      newRows.push(schedule);
      setRows(newRows);
      console.log("Adding new schedule to array");
      console.log(rows);
      //setRowsJson(JSON.stringify(rows));
      axiosGcp.post("/addInterval", { id: schedule.id, start_date: schedule.start, end_date: schedule.end })
        .then((response) => {
          console.log(response);
          // if (response.status == 500) {
          //   //persons.splice(persons.findIndex(item => item.firstName === 'William'),1);
          //   rows.splice(rows.findIndex(item => item.id === response.data.id), 1);
          //   console.log("Removed element with id: " + response.data.id);
          //   setRowsJson(JSON.stringify(rows));
          //   //rows = JSON.parse(rowsJson);
          // }
        }).catch((err) => {
          console.log(err);
          rows.splice(rows.findIndex(item => item.id === schedule.id), 1);
          console.log("Removed element with id: " + schedule.id);
          //setRowsJson(JSON.stringify(rows));
        })
    }
  }

  // let schedules = [{
  //   id: 1000000000,
  //   start: "response.data[index].start_date",
  //   end: "response.data[index].end_date",
  // }];
  //let [rows, setRows] = React.useState(new Array<Schedule>());



  return (
    <Grid
      container
      margin={10}
      alignItems="center"
      direction="column"
      width={"90vw"}
      height={"90vh"}
      style={{
        //background: "linear-gradient(to right top, #4358cb, #c951bd)"
        background: "#ffffff",
      }}
    >
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <div>
          <DateRangePicker
            calendars={1}
            value={value}
            onChange={(newValue) => {
              setValue(newValue);
            }}
            renderInput={(startProps, endProps) => (
              <React.Fragment>
                <TextField {...startProps} />
                <TextField {...endProps} />
              </React.Fragment>
            )}
          />
        </div>
      </LocalizationProvider>
      <DataGrid rows={rows} columns={columns} checkboxSelection />
      <Button onClick={submit}> Submit</Button>
    </Grid>
  );
};

export default DatePickerPage;
