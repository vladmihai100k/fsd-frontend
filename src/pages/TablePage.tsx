import { useContext } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { Grid, Button } from "@mui/material";
import { logout } from "../hooks/useAuth";
import AuthContext from "../DataContexts";
import useLocalStorage from "../hooks/useLocalStorage";
import { ImagePrediction } from "../entity/ImagePrediction";

const columns = [
  { field: "id", headerName: "ID", hide: true },
  { field: "name", headerName: "Image Name", flex: 1 },
  { field: "size", headerName: "Size", flex: 1 },
  { field: "prediction", headerName: "Recognition Result", flex: 1 },
  { field: "imgSrc", headerName: "Image Download Link", flex: 1 },
];

const rows = [
  {
    id: 1,
    imageName: "Spring",
    size: "1600x1067",
    recognitionResult: "?",
    imageLink:
      "https://cdn.britannica.com/05/155405-050-F8969EE6/Spring-flowers-fruit-trees-bloom.jpg",
  },
  {
    id: 2,
    imageName: "Summer",
    size: "926x614",
    recognitionResult: "?",
    imageLink:
      "https://images.theconversation.com/files/20706/original/dkkvsc9j-1361934641.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=926&fit=clip",
  },
  {
    id: 3,
    imageName: "Autumn",
    size: "800x533",
    recognitionResult: "?",
    imageLink:
      "https://cdn.britannica.com/88/137188-050-8C779D64/Boston-Public-Garden.jpg",
  },
  {
    id: 4,
    imageName: "Winter",
    size: "2400x1600",
    recognitionResult: "?",
    imageLink:
      "https://www.almanac.com/sites/default/files/styles/primary_image_in_article/public/image_nodes/winter_sunrise-2.jpg?itok=r0JqLvVz",
  },
  {
    id: 5,
    imageName: "Dog",
    size: "1200x602",
    recognitionResult: "?",
    imageLink:
      "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=1.00xw:0.669xh;0,0.190xh&resize=768:*",
  },
];
const TablePage = () => {
  const { authenticated, setAuthenticated } = useContext(AuthContext);
  const [images, setImages] = useLocalStorage("images", JSON.stringify([]));
  const newRows: Array<ImagePrediction> = JSON.parse(images);
  console.log(newRows);
  return (
    <Grid container bgcolor={"#cccccc"}>
      <div
        style={{
          height: "80vh",
          width: "90vw",
          margin: "5%",
          backgroundColor: "#ffffff",
        }}
      >
        <DataGrid rows={newRows} columns={columns} checkboxSelection />
      </div>
      <Button
        variant="contained"
        style={{ backgroundColor: "#54bc44" }}
        size={"large"}
        sx={{ minWidth: "100%" }}
        onClick={() => {
          //setAuthenticated(true);
          //alert(username + " " + password);
          logout(setAuthenticated);
        }}
      >
        Logout
      </Button>
    </Grid>
  );
};

export default TablePage;
